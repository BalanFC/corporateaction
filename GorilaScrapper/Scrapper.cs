﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using HtmlAgilityPack;
using System.Collections;
using System.Web;
using System.Collections.Specialized;
using System.Configuration;

namespace GorilaScrapper
{
    public static class Scrapper
    {
        // Scraper - Buscar os dados do site BVM - Proventos em Ativo
        public static List<RawB3CorporateAction> ListarProventoAtivo(string urlRoot, int CD_CVM)
        {
            HtmlWeb web = new HtmlWeb();
            var url = string.Format("{0}/cias-Listadas/Empresas-Listadas/ResumoEventosCorporativos.aspx?codigoCvm={1}&tab=3.0&idioma=pt-br",
                urlRoot,
                CD_CVM);
            HtmlDocument doc = web.Load(url);

            var headers = doc.DocumentNode.SelectNodes("//table[@id='ctl00_contentPlaceHolderConteudo_grdBonificacao_ctl01']//tr/th");

            //Cria novo objeto de provento ativo
            List<RawB3CorporateAction> itemProventos = new List<RawB3CorporateAction>();

            if (headers != null) //Verificar se tem dados ou não
            {
                foreach (var row in doc.DocumentNode.SelectNodes("//table[@id='ctl00_contentPlaceHolderConteudo_grdBonificacao_ctl01']//tr[td]"))
                {
                    var valores = row.SelectNodes("td").Select(td => td.InnerText).ToArray();
                    RawB3CorporateAction x = new RawB3CorporateAction();

                    x.CorporateActionType = valores[0];
                    x.IssuerStock = (valores[1].RemoveEspacos() == "") ? null : valores[1].Substring(2, 4) + valores[1].Substring(11, 1);
                    x.IssuedStock = (valores[5].RemoveEspacos() == "") ? null : valores[5].Substring(2, 4) + valores[5].Substring(11, 1);
                    x.AnnouncementDate = valores[2];
                    x.CumDate = valores[3].RemoveEspacos();
                    x.PaymentDate = null;
                    x.Factor = valores[4].RemoveEspacos();
                    x.Value = "0";

                    itemProventos.Add(x);
                }
            }
            else
            {
                itemProventos = null;
            }
            return itemProventos;
        }

        // Scraper - Buscar os dados do site BVM - Proventos em Dinheiro
        public static List<RawB3CorporateAction> ListarProventoDinheiro(string urlRoot, int CD_CVM)
        {
            HtmlWeb web = new HtmlWeb();
            var url = string.Format("{0}/cias-Listadas/Empresas-Listadas/ResumoProventosDinheiro.aspx?codigoCvm={1}&tab=3.1&idioma=pt-br",
                urlRoot,
                CD_CVM);
            HtmlDocument doc = web.Load(url);

            var headers = doc.DocumentNode.SelectNodes("//table[@id='ctl00_contentPlaceHolderConteudo_grdProventos_ctl01']//tr/th");

            //------------------------------------------------------------------------------------------------------------------------------------------------------------------
            // Query = codigo de CVM >  Pegar os dados da lista de emissor x cadastral csv para ter campo Emissor, pois o emissor tem que juntar com o campo tipo de ativo
            var dtListCVM = ListaCadastralCSV().Join(ListaEmissor(),
                      outerKey => outerKey.CNPJ_CIA,
                      innerKey => innerKey.CNPJ,
                      (CadastralCSV, Emissor) => new
                      {
                          Emissor.EMISSOR,
                          Emissor.EMPRESA,
                          CadastralCSV.CNPJ_CIA,
                          CadastralCSV.CD_CVM,

                      }).Where(x => x.CD_CVM == CD_CVM.ToString())
                      .ToList();
            //------------------------------------------------------------------------------------------------------------------------------------------------------------------

            //Cria novo objeto de provento ativo
            List<RawB3CorporateAction> itemProventos = new List<RawB3CorporateAction>();

            if (headers != null) //Verificar se tem dados ou não
            {
                foreach (var row in doc.DocumentNode.SelectNodes("//table[@id='ctl00_contentPlaceHolderConteudo_grdProventos_ctl01']//tr[td]"))
                {
                    var valores = row.SelectNodes("td").Select(td => td.InnerText).ToArray();
                    RawB3CorporateAction x = new RawB3CorporateAction();


                    x.CorporateActionType = valores[4];
                    x.IssuerStock = (valores[0] == "&nbsp;") ? null :
                                            (valores[0] == "ON") ? dtListCVM[0].EMISSOR + "3" :
                                            (valores[0] == "PN") ? dtListCVM[0].EMISSOR + "4" :
                                            (valores[0] == "PNA") ? dtListCVM[0].EMISSOR + "5" :
                                            (valores[0] == "PNB") ? dtListCVM[0].EMISSOR + "6" :
                                            (valores[0] == "PNC") ? dtListCVM[0].EMISSOR + "7" :
                                            (valores[0] == "PND") ? dtListCVM[0].EMISSOR + "8" :
                                            (valores[0] == "UNT") ? dtListCVM[0].EMISSOR + "11" : "Undefined_" + valores[0];
                    x.IssuedStock = null;
                    x.AnnouncementDate = valores[1];
                    x.CumDate = valores[5];
                    x.PaymentDate = null;
                    x.Factor = "0";
                    x.Value = valores[3];

                    itemProventos.Add(x);

                }
            }
            else
            {
                itemProventos = null;
            }
            return itemProventos;
        }

        #region All classes to list each file

        // Mostrar os dados da lista de arquivos na pagina Geração de arquivos
        public static List<Arquivos> ListaArquivo()
        {
            DataTable tblArquivo = new DataTable();

            tblArquivo = ConvertFilesToDataTable();
            List<Arquivos> itemArquivo = new List<Arquivos>();

            itemArquivo = tblArquivo.AsEnumerable()
                          .Select(row => new Arquivos
                          {
                              FileName = row.Field<string>("FileName"),
                              FilePath = row.Field<string>("FilePath")
                          }).ToList();

            return itemArquivo;
        }

        // Gerar os dados de Emissor TEXT (tabela) para pegar os dados na classe Emissor.cs
        public static List<Emissor> ListaEmissor()
        {
            List<Emissor> itemEmissor = new List<Emissor>();

            //pega os dados da lista de arquivos
            List<Arquivos> arquivo = ListaArquivo().ToList().FindAll(x => x.FileName.IndexOf(ConfigurationManager.AppSettings["txt"], StringComparison.OrdinalIgnoreCase) >= 0);

            // Verifica se existe arquivo na lista de arquivos
            if (arquivo.Count > 0)
            {
                var path = arquivo.Find(x => x.FileName.IndexOf(ConfigurationManager.AppSettings["txt"], StringComparison.OrdinalIgnoreCase) >= 0).FilePath;

                //pega os dados da lista Cadastral CSV
                DataTable tblEmissor = new DataTable();
                tblEmissor = ConvertEmissorToDataTable(path);

                itemEmissor = tblEmissor.AsEnumerable()
                              .Select(row => new Emissor
                              {
                                  EMISSOR = row.Field<string>("Column1"),
                                  EMPRESA = row.Field<string>("Column2"),
                                  CNPJ = row.Field<string>("Column3"),
                                  DTEMISSOR = row.Field<string>("Column4")
                              }).ToList();
            }
            else
            {
                itemEmissor = null;
            }

            return itemEmissor;
        }

        // Gerar os dados de Cadastral CSV (tabela) para pegar os dados na classe CadastralCSV.cs
        public static List<CadastralCSV> ListaCadastralCSV()
        {
            List<CadastralCSV> itemCadastral = new List<CadastralCSV>();

            //pega os dados da lista de arquivos
            List<Arquivos> arquivo = ListaArquivo().ToList().FindAll(x => x.FileName.IndexOf(ConfigurationManager.AppSettings["csv"], StringComparison.OrdinalIgnoreCase) >= 0);

            // Verifica se existe arquivo na lista de arquivos
            if (arquivo.Count > 0)
            {
                var path = arquivo.Find(x => x.FileName.IndexOf(ConfigurationManager.AppSettings["csv"], StringComparison.OrdinalIgnoreCase) >= 0).FilePath;

                //pega os dados da lista Cadastral CSV
                DataTable dtCadastral = new DataTable();
                dtCadastral = ConvertCSVtoDataTable(path);

                itemCadastral = dtCadastral.AsEnumerable()
                              .Select(row => new CadastralCSV
                              {
                                  // assuming column 0's type is Nullable<long>
                                  CNPJ_CIA = row.Field<string>("CNPJ_CIA"),
                                  CD_CVM = row.Field<string>("CD_CVM"),
                              }).ToList();
            }
            else
            {
                itemCadastral = null;
            }

            return itemCadastral;
        }

        #endregion      

        #region Manipulate the data of each file

        // Buscar os dados da lista de arquivos
        public static DataTable ConvertFilesToDataTable()
        {
            DataTable FilesTable = new DataTable();
            FilesTable.Columns.Add("FileName");
            FilesTable.Columns.Add("FilePath");

            string yourPath = System.Web.HttpContext.Current.Server.MapPath("~/Upload/");
            string searchPattern = "*.*";
            var resultData = Directory.GetFiles(yourPath, searchPattern, SearchOption.AllDirectories)
                            .Select(x => new { FileName = Path.GetFileName(x), FilePath = x });

            foreach (var item in resultData)
            {
                DataRow dRow = FilesTable.NewRow();
                dRow["FileName"] = item.FileName;
                dRow["FilePath"] = item.FilePath;

                FilesTable.Rows.Add(dRow);
            }

            return FilesTable;
        }

        // Converter Emissor TEXT para tabela (manipular os dados)
        public static DataTable ConvertEmissorToDataTable(string filePath)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(filePath))
            {
                string[] headers = sr.ReadLine().Split(',');

                ArrayList al1 = new ArrayList();
                al1.AddRange(headers);

                for (int col = 0; col < al1.Count; col++)
                {
                    dt.Columns.Add("Column" + (col + 1).ToString());

                }

                while (!sr.EndOfStream)
                {
                    var msg = sr.ReadLine().Replace("\"", "");
                    var rows = msg.Split(',');

                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }

            return dt;
        }

        // Converter Cadastral CSV para tabela (manipular os dados)
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(strFilePath))
            {
                string[] headers = sr.ReadLine().Split(';');
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(';');
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        if (rows[i] == rows[0])
                        {
                            dr[i] = SemFormatacao(rows[i]);
                        }
                        else
                            dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }

            return dt;
        }

        #endregion

        #region All Classes

        public class RawB3CorporateAction
        {
            public string CorporateActionType { get; set; }
            public string IssuerStock { get; set; }
            public string IssuedStock { get; set; }
            public string AnnouncementDate { get; set; }
            public string CumDate { get; set; }
            public string PaymentDate { get; set; }
            public string Factor { get; set; }
            public string Value { get; set; }
        }

        public class CadastralCSV
        {
            public string CNPJ_CIA { get; set; }
            public string DENOM_SOCIAL { get; set; }
            public string DENOM_COMERC { get; set; }
            public string DT_REG { get; set; }
            public string DT_CONST { get; set; }
            public string DT_CANCEL { get; set; }
            public string MOTIVO_CANCEL { get; set; }
            public string SIT { get; set; }
            public string DT_INI_SIT { get; set; }
            public string CD_CVM { get; set; }
            public string SETOR_ATIV { get; set; }
            public string CATEG_REG { get; set; }
            public string DT_INI_CATEG { get; set; }
            public string SIT_EMISSOR { get; set; }
            public string DT_INI_SIT_EMISSOR { get; set; }
            public string TP_ENDER { get; set; }
            public string LOGRADOURO { get; set; }
            public string COMPL { get; set; }
            public string BAIRRO { get; set; }
            public string MUN { get; set; }
            public string UF { get; set; }
            public string PAIS { get; set; }
            public string CEP { get; set; }
            public string DDD_TEL { get; set; }
            public string TEL { get; set; }
            public string DDD_FAX { get; set; }
            public string FAX { get; set; }
            public string EMAIL { get; set; }
            public string TP_RESP { get; set; }
            public string RESP { get; set; }
            public string DT_INI_RESP { get; set; }
            public string LOGRADOURO_RESP { get; set; }
            public string COMPL_RESP { get; set; }
            public string BAIRRO_RESP { get; set; }
            public string MUN_RESP { get; set; }
            public string UF_RESP { get; set; }
            public string PAIS_RESP { get; set; }
            public string CEP_RESP { get; set; }
            public string DDD_TEL_RESP { get; set; }
            public string TEL_RESP { get; set; }
            public string DDD_FAX_RESP { get; set; }
            public string FAX_RESP { get; set; }
            public string EMAIL_RESP { get; set; }
            public string CNPJ_AUDITOR { get; set; }
            public string AUDITOR { get; set; }
        }

        public class Emissor
        {
            public string EMISSOR { get; set; }
            public string EMPRESA { get; set; }
            public string CNPJ { get; set; }
            public string DTEMISSOR { get; set; }
        }

        public class Arquivos
        {
            public string FileName { get; set; }
            public string FilePath { get; set; }
        }

        #endregion

        #region Cleaner "Blank"

        //Função para limpar os espacos em branco e os \r\n que vem do HTML
        private static string RemoveEspacos(this string texto)
        {
            return texto.Replace(System.Environment.NewLine, string.Empty).Trim();
        }

        #endregion

        #region Format CNPJ

        public static string SemFormatacao(string Codigo)
        {
            return Codigo.Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty);
        }      

        #endregion
    }
}
