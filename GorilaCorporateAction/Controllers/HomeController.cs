﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HtmlAgilityPack;
using System.Collections;
using System.Text;
using System.Configuration;
using GorilaScrapper;

namespace GorilaCorporateAction.Controllers
{
    public class HomeController : Controller
    {
        #region View Pages

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GeracaoArquivo()
        {
            return View();
        }

        #endregion

        #region View List on page - Geração de Arquivos

        [HttpPost]
        public JsonResult FilesList() // Lista de Arquivos
        {
            var data = Scrapper.ListaArquivo().ToList();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveFile() // Subir arquivos (Upload)
        {
            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var pic = System.Web.HttpContext.Current.Request.Files["HelpSection"];
                    HttpPostedFileBase filebase = new HttpPostedFileWrapper(pic);

                    var fileName = Path.GetFileName(filebase.FileName);

                    //Nome da pasta completa
                    var path = Path.Combine(Server.MapPath("~/Upload/"), fileName);

                    //Salva arquivo para pasta
                    filebase.SaveAs(path);

                    return Json("File Saved Successfully.");
                }
                else
                {
                    return Json("No File Saved.");
                }
            }
            catch (Exception ex)
            {
                return Json("Error While Saving.");
            }
        }

        #endregion

        #region View list on page - Consulta

        public ActionResult GetData() // Mostrar os dados da lista Emissor x Cadastral na página Home/Index
        {
            if (Scrapper.ListaCadastralCSV() != null && Scrapper.ListaEmissor() != null)
            {
                var lista = Scrapper.ListaCadastralCSV().Join(Scrapper.ListaEmissor(),
                         outerKey => outerKey.CNPJ_CIA,
                         innerKey => innerKey.CNPJ,
                         (CadastralCSV, Emissor) => new
                         {
                             Emissor.EMISSOR,
                             Emissor.EMPRESA,
                             CadastralCSV.CNPJ_CIA,
                             CadastralCSV.CD_CVM,

                         }).ToList();

                return Json(new { data = lista }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string data = null;
                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ProventoAtivo(int CD_CVM) // Mostrar os dados da lista Proventos em Ativo na página Index
        {
            var data = Scrapper.ListarProventoAtivo(ConfigurationManager.AppSettings["url"], CD_CVM);

            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ProventoDinheiro(int CD_CVM) // Mostrar os dados da lista Proventos em Dinheiro na página Index
        {       
            var data = Scrapper.ListarProventoDinheiro(ConfigurationManager.AppSettings["url"], CD_CVM);

            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        #endregion 
        
    }
}